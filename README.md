# The Proxy Pattern with Unstructured Storage

The implementation of the Proxy Pattern with Unstructured Storage to upgrade a 
simple smart contract

### Prerequisites

```
git clone https://gitlab.com/ostannah/upgradeability-strategy.git
npm install npm install -g truffle
npm install -g ethereumjs-testrpc
```

### Compiling

```
truffle compile
```

## Running the Ethereum client
```
testrpc
```

## Running the tests

```
truffle test
```

## Built With

* [truffle](https://www.trufflesuite.com/) - The framework used
* [rpc](https://www.npmjs.com/package/ethereumjs-testrpc) - Node.js based Ethereum client for testing and development

## Authors

* **Olivia Stannah**


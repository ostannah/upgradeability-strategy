pragma solidity ^0.4.21;

contract MathsInterface {
    function add() public;
    function subtract(uint number) public;
    function sum() public view returns(uint);
}
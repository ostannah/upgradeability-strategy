pragma solidity ^0.4.21;

import "./MathsInterface.sol";

contract MathsUpgraded is MathsInterface {
    mapping (address => uint) value;

    function add() public {
        value[msg.sender] = value[msg.sender] + 10;
    }

    function subtract(uint number) public {
        value[msg.sender] = value[msg.sender] - 10 - number;
    }

    function sum() public view returns(uint) {
        return value[msg.sender];
    }
}
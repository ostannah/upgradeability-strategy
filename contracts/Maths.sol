pragma solidity ^0.4.21;

import "./MathsInterface.sol";

contract Maths is MathsInterface {
    mapping (address => uint) value;

    function add() public {
        value[msg.sender] = value[msg.sender] + 5;
    }

    function subtract(uint number) public {
        value[msg.sender] = value[msg.sender] - 5 - number;
    }

    function sum() public view returns(uint) {
        return value[msg.sender];
    }
}
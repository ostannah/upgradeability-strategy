pragma solidity ^0.4.21;

contract Proxy {
    bool[] private votes;
    address private logic_contract;
    address private administrator;
    address private movie_owner;
    address private requestedUpgrade;

    bytes32 constant STORAGE_SLOT_HASH = keccak256('OliviaImplementationSlot');

    constructor(address _address) public {
        administrator = msg.sender;
        require(getStorageSlot() == address(0), "storage slot isnt free");
        setStorageSlot(_address);
    }

    function setStorageSlot(address _address) public {
        require(_address != address(0));
        logic_contract = _address;
        bytes32 slot = STORAGE_SLOT_HASH;
        assembly {
            sstore(slot, _address)
        }
    }

    function getStorageSlot() public view returns (address _address) {
        bytes32 slot = STORAGE_SLOT_HASH;
        assembly {
            _address := sload(slot)
        }
    }

    function () public {
        address target = getStorageSlot();
        assembly {
            let ptr := mload(0x40)
            calldatacopy(ptr, 0, calldatasize)
            let result := delegatecall(gas, target, ptr, calldatasize, 0, 0)
            let size := returndatasize
            returndatacopy(ptr, 0, size)

            switch result
            case 0 { revert(ptr, size) }
            default { return(ptr, size) }
        }

    }

    function setMovieOwner(address _address) public onlyAdministrator {
        movie_owner = _address;
    }

    function submitUpgradeAdmin(address _address) public onlyAdministrator {
        setStorageSlot(_address);
    }

    function submitUpgradeMovieOwner() public onlyMovieOwner {
        for (uint i = 0; i < votes.length; i++) {
            require(votes[i] != false, "update cannot be submitted, not enough votes");
        }
        setStorageSlot(requestedUpgrade);
    }

    function getRequestedUpgrade() public  view returns (address _address) {
        _address = requestedUpgrade;
    }

    function requestUpgrade(address _address) public onlyMovieOwner {
        requestedUpgrade = _address;
        votes = new bool[](0);
    }

    function castVote(bool _vote) public {
        votes.push(_vote);
    }

    // function getVotes() public view returns (bool[]) {
    //     return votes;
    // }

    modifier onlyAdministrator() {
        require(msg.sender == administrator, "only administrator can submit upgrade");
        _;
    }

    modifier onlyMovieOwner() {
        require(msg.sender == movie_owner, "only movie owner can submit an approved upgrade");
        _;
    }

}

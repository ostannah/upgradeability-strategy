const Maths = artifacts.require('./Maths.sol');
const MathsUpgraded = artifacts.require('./MathsUpgraded.sol');
const MathsInterface = artifacts.require("MathsInterface.sol");
const Proxy = artifacts.require("Proxy.sol")

var maths;
var proxy;
var mathsInterface;
var mathsUpgraded;
var administrator;

// Please note that as the storage values are preserved, the previous value of the state varaibles are inherited by each test

// Please note that go demonstrate an upgrade, upgrades occur in BOTH directions (from maths to mathsUpgraded and vice versa)

// The maths contract adds 5 when add() is called, and the maths upgraded contract adds 10 when add() is called

contract('Maths Test', function (accounts) {
    before(async function () {

        administrator = accounts[0]

        // Deploys initial logic contract
        maths = await Maths.new({ from: administrator });

        // Deploys proxy contract
        proxy = await Proxy.new(maths.address, { from: administrator });

        // Creates interface
        mathsInterface = await MathsInterface.at(proxy.address, { from: administrator });
    })

    describe('Checks unstructure storage slot', function () {
        it("initial logic contract is stored in unstructured storage slot", async function () {

            assert.equal(await proxy.getStorageSlot({ from: administrator }), maths.address);

        })
    })

    describe('Testing initial logic contract', function () {
        it("adds 5 each time", async function () {

            const player = accounts[1];

            await mathsInterface.add({ from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 5);

            await mathsInterface.add({ from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 10);

            await mathsInterface.add({ from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 15);

        })
    })

    describe('Testing initial logic contract', function () {
        it("subtracts 5 plus parameter each time", async function () {

            const player = accounts[1];

            await mathsInterface.subtract(2, { from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 8);

            await mathsInterface.subtract(1, { from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 2);

        })
    })

    describe('Upgrading the logic contract', function () {
        it("deploys new logic contract", async function () {

            mathsUpgraded = await MathsUpgraded.new({ from: administrator });
            await proxy.setStorageSlot(mathsUpgraded.address, { from: administrator });

        })

        it("new logic contract is stored in unstructured storage slot", async function () {

            assert.equal(await proxy.getStorageSlot({ from: administrator }), mathsUpgraded.address);

        })
        it("adds 10 each time", async function () {

            const player = accounts[1];

            await mathsInterface.add({ from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 12);

            await mathsInterface.add({ from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 22);

            await mathsInterface.add({ from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 32);

        })

        it("subtracts 10 plus parameter each time", async function () {

            const player = accounts[1];

            await mathsInterface.subtract(9, { from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 13);

            await mathsInterface.subtract(3, { from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 0);

        })
    })

    describe('Governance strategy permissions', function () {
        it("allows administrator to freely submit upgrade", async function () {
            const player = accounts[1];

            maths = await Maths.new({ from: administrator });
            await proxy.submitUpgradeAdmin(maths.address, { from: administrator });

            await mathsInterface.add({ from: player });
            assert.equal(await mathsInterface.sum({ from: player }), 5);

        })

        it("does not allow other accounts to freely submit upgrade ", async function () {

            const player = accounts[1];

            mathsUpgraded = await MathsUpgraded.new({ from: administrator });

            try {
                await proxy.submitUpgradeAdmin(mathsUpgraded.address, { from: player });
                assert(false); 
            } catch (error) {
                assert.ok(error);
            }

        })

        it("allows movie owner to request upgrade ", async function () {

            const movieOwner = accounts[1];

            await proxy.setMovieOwner(movieOwner, {from: administrator});

            await proxy.requestUpgrade(mathsUpgraded.address, ({from: movieOwner}));

            assert.equal(await proxy.getRequestedUpgrade({ from: administrator }), mathsUpgraded.address);
        
        })

        it("does not allow other accounts to request upgrade ", async function () {

            const player = accounts[2];

            try {
                await proxy.requestUpgrade(mathsUpgraded.address, ({from: player}));
                assert(false); 
            } catch (error) {
                assert.ok(error);
            }

        })
    })

    describe('Governance strategy voting mechanism', function () {
        it("submit requested upgrade passes if all votes are true ", async function () {

            const movieOwner = accounts[1];
            const player1 = accounts[2];
            const player2 = accounts[3];
            const player3 = accounts[4];

            mathsUpgraded = await MathsUpgraded.new({ from: administrator });

            await proxy.setMovieOwner(movieOwner, {from: administrator});
            await proxy.requestUpgrade(mathsUpgraded.address, ({from: movieOwner}));

            await proxy.castVote(true, ({from: player1}));
            await proxy.castVote(true, ({from: player2}));
            await proxy.castVote(true, ({from: player3}));

            await proxy.submitUpgradeMovieOwner({from: movieOwner});

           assert(proxy.getStorageSlot({from: administrator}), mathsUpgraded.address);
        })

        it("submit requested upgrade if one votes is false ", async function () {

            const movieOwner = accounts[1];
            const player1 = accounts[2];
            const player2 = accounts[3];
            const player3 = accounts[4];

            mathsUpgraded = await MathsUpgraded.new({ from: administrator });

            await proxy.setMovieOwner(movieOwner, {from: administrator});
            await proxy.requestUpgrade(mathsUpgraded.address, ({from: movieOwner}));

            await proxy.castVote(1, ({from: player1}));
            await proxy.castVote(1, ({from: player2}));
            await proxy.castVote(1, ({from: player3}));

            try {
                await proxy.submitUpgradeMovieOwner({from: movieOwner});
                assert(false); 
            } catch (error) {
                assert.ok(error);
            }
        })
    })
})